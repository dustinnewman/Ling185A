# Week Three, Discussion: Higher-Order Functions

A **higher-order function** takes a function as an argument.

- ex: `map/2`, `filter/2`

## Problems

```haskell
applyToAll :: (a -> a) -> [a] -> [a]
applyToAll _ [] = []
applyToAll f (h:t) = (f h):(applyToAll f t)

-- |
-- >>> suffixes [1,2,3]
-- [[1,2,3],[2,3],[3],[]]
suffixes :: [a] -> [[a]]
suffixes [] = [[]]
suffixes (h:t) = (h:t):(suffixes t)

-- |
-- >>> beforeAndAfter (+1) [1,3,5]
-- [(1,2),(3,4),(5,6)]
beforeAndAfter :: (a -> b) -> [a] -> [(a,b)]
beforeAndAfter _ [] = []
beforeAndAfter f (h:t) = (h,f h):(beforeAndAfter f t)

-- |
-- >>> inverseMap [(+2),(+3),(+4)] 5
-- [7,8,9]
inverseMap :: [(a -> b)] -> a -> [b]
inverseMap [] _ = []
inverseMap (hf:tf) x = (hf x):(inverseMap tf x)
```

```haskell
-- |
-- >>> countEvens [[1,2,4,6],[2,4,6,7,8],[2,3]]
-- [3,4,1]
countEvens :: [[a]] -> [Int]
countEvens l = map (\x -> [length (filter even x)]) l
```


