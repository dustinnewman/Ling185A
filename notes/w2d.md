# Week Two, Discussion

## Recursion

In Haskell we can have either recursive *types* or *functions*

- Recursive types: A type which contains an instance of itself

- Recursive functions: A function which calls itself

But it's important to have a **base case** which terminates the recursion.

ex: `Numb`

```haskell
data Numb = E | S Numb deriving Show

isEven :: Numb -> Bool
isEven E = True
isEven (S n) = not (isEven n)


isOdd :: Numb -> Bool
isOdd n = case isEven n of 
    True -> False
    False -> True
```

ex: `multiply`

```haskell
multiply :: [Int] -> Int
multiply [] = 1
multiply (h : t) = h * (multiply t)
```

ex: `fib`

```haskell
fib :: Int -> Int
fib 0 = 1
fib 1 = 1
fib n = fib (n - 1) + fib (n - 2)

-- Over custom Numb type
fib :: Numb -> Numb
fib E = (S E)
fib (S E) = (S E)
fib (S (S n)) = add (fib (S n)) (fib n)
-- We need `add` from lecture
add :: Numb -> (Numb -> Numb)
add a E = a -- a + 0 = a
add E b = b -- 0 + b = b
add (S a) (S b) = S (S (add a b))
```

ex: `sub`

```haskell
sub :: Numb -> (Numb -> Numb)
sub a E = a -- a - 0 = a
sub E b = E -- 0 - b = 0
sub (S a) (S b) = sub a b
```


