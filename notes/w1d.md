# Week One, Discussion

## Lambda Calculus

Haskell is based on lambda calculus from Alonzo Church and Stephen Kleene in the 1930s.

- describe functions in an unambiguous and compact manner
- functions are *values* and can be arguments to other functions

In lambda calculus, **reductions** reduce a statement to its simplest possible form.

- lambda reductions => `let` in Haskell
- case reductions

Parentheses are used to indicate the scope of the lambda.

- ex: `(\x -> x + 1)` is a lambda
- but `(\x -> x + 1) 4` is applied and evaluates to 5
- `(\x -> (\y -> x * 3 + y)(3))5 => 18`
- `((\x -> (\y -> y + (3 * x)))4)2 => 14`

## Evaluations

```
n = 1
(\x -> (3+x)*n) 2

=> (3 + 2) * n  lambda reduction
=> 5 * n        arithmetic
=> 5 * 1        substitution
=> 5            arithmetic
```

```
x = 3 + 5 in x * 4

=> (3 + 5) * 4  let reduction
=> 8 * 4        arithmetic
=> 32           arithmetic
```

```
y = (\x -> x + 5) in y((\x -> case x of 6 -> 2; 5 ->4; 8 -> 6) 5)

=> (\x -> x + 5) ((\x -> case x of 6 -> 2; 5 -> 4; 8 -> 6) 5)           let reduction
=> (\x -> x + 5) (case 5 of 6 -> 2; case 5 of 5 -> 4; case 5 of 8 -> 6) lambda reduction
=> (\x -> x + 5) (4)                                                    case reduction
=> 4 + 5                                                                lambda reduction
=> 9                                                                    arithmetic
```

```
Assume:
k = \x -> case x of 9 -> 1; 8 -> 3; 7 -> 2

(\x -> (\y -> x * 3 + y) (k 8)) 4

=> (\x -> (\y -> x * 3 + y) ((\x -> case x of 9 -> 1; 8 -> 3; 7 -> 2) 8)) 4     substitution
=> (\x -> (\y -> x * 3 + y) (case 8 of 9 -> 1; 8 -> 3; 7 -> 2) 4)               lambda reduction
=> (\x -> (\y -> x * 3 + y) 3) 4                                                case reduction
=> (\y -> 4 * 3 + y) 3                                                          lambda reduction
=> 4 * 3 + 3                                                                    lambda reduction
=> 12 + 3                                                                       arithmetic
=> 15                                                                           arithmetic
```

Represent an expression `e` whose value depends on a shape represented by the variable `s` and a number represented by variable `x`

- If the shape `s` is `Rock`, then `e` evalutates to the square of the number `x`
- If the shape `s` is `Papers`, then `e` evaluates to the cube of the number `x`
- If the shape `s` is `Scissors`, then `e` evaluates to the number `x` itself

```
(\s -> case s of Rock -> (\x -> x * x); Paper -> (\x -> x * x * x); Scissors -> (\x -> x))
```
