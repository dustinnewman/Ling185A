# Week Four, Discussion: FSM 🤖
A **formal language** is a language defined by computable formulas. Formal Language Theory was heavily influenced by Noam Chomsky, who was trying to see if human, natural languages were computable. In this, he created the **Chomsky Hierarchy** to rank the different computable languages.

The lowest value in the hierarchy is **regular languages** - recognizable by finite state machines (FSM) and regular expressions.
- an FSM is used to *recognize* patterns of strings
    - we have two flavors: deterministic and nondeterministic
- a RegEx is used to *generate* patterns of strings
    - closed under the union 🦄, concatenation 🐱, and Kleene star 🌟 operations

## Exercise 1
1. Yes
2. Yes
3. No
4. No

## Exercise 2
"Banana"
(Q = {q0,q1,qA}, Σ = {b,a,n}, q<sub>0</sub> = q0, F = {qA})
δ =

| Symbol |  q0  |  q1  |  qA  |
|--------|------|------|------|
|   b    |  q1  |      |      |
|   a    |      |  qA  |      |
|   n    |      |      |  q1  |

Dates
(Q = {q0,q1,q2,q3,q4}, Σ = {of, January...December, 1st...31st, Christmas, Halloween, New Years, ...}, q<sub>0</sub> = q0, F = {q3})
δ = 

|           Symbol          | q0 | q1 | q2 | q3 | q4 |
|---------------------------|----|----|----|----|----|
| January, ... ,December    | q1 |    |    |    | q3 |
| 1st...31st                | q2 | q3 |    |    |    |
| Christmas, Halloween, ... | q3 |    |    |    |    |
| of                        |    |    | q4 |    |    |

## Exercise 4: Gumball Machine 🍬
1. Gumballs cost 50 cents
2. Gumball machines take nickels, dimes, and quarters
3. The FSM must take as input a series of these coin values
4. The FSM must reach an accepting state iff 50 *or more* cents have been put in
5. Don't give change back

(Q = {q0, q5, q10, q15, q20, q25, q30, q35, q40, q45, q50}, Σ = {5, 10, 25}, q<sub>0</sub> = q0, F = {q50})
δ =

| ¢  | q0  | q5  | q10 | q15 | q20 | q25 | q30 | q35 | q40 | q45 | q50 |
|----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
| 5  | q5  | q10 | q15 | q20 | q25 | q30 | q35 | q40 | q45 | q50 |     |
| 10 | q10 | q15 | q20 | q25 | q30 | q35 | q40 | q45 | q50 | q50 |     |
| 25 | q25 | q30 | q35 | q40 | q45 | q50 | q50 | q50 | q50 | q50 |     |

