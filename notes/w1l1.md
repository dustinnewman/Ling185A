# Week One, Lecture One

## Haskell Basics

Haskell is a *functional* language, contrasting with imperative languages like C or Python.

- functional: programs are the result of the evaluation of functions
- imperative: programs are the result of the successive evaluation of instructions

Haskell uses *pure functions* where the result of every function are determinable and rely solely on the input arguments.

Haskell is *statically-typed*, ensuring that code remains type safety.

Haskell uses *lazy evaluation*, evaluating expressions only when needed.

Haskell uses *immutable data*. In contrast to C, where you can bind a value to a variable `x`, use `x`, and reassign `x`, in Haskell `x` is immutable and only assignable at definition.

We use Haskell over OCaml because Haskell emphasizes *structure* and custom types and the relationship between those types, whereas OCaml emphasizes *logic* and transformations from functions.

Use the command `ghci` to start the Glasgow Haskell Compiler Interactive program (basically Haskell REPL). We will only need `ghci` in this course.

- `:l` loads a file into ghci
- `:r` reloads any loaded files
- `:q` exits ghci
- `:t` prints the type of an expression

## Expressions in Haskell

Haskell does **not** have statements! Everything in Haskell is either an **expression** or a **function**.

An **expression** is a value or the result of the evaluation of a function.

- ex: 3, 3+4, "foo", x/42 are all expressions

To represent a single step of evaluation, we use the `=>` notation.

- ex: 42 + 5 => 47
- but 2 \* 2 + 5 => 4 + 5 because 2 times 2 is just one step

To represent the final evaluation, which may be zero or more steps, we use `=>*` (**Kleene star**) notation. The Kleene star notation is:

- reflexive: `x =>* x` is always true
- transitive `x =>* y` iff `Ex'[ x => x and x' =>* y]` (x Kleene-evaluates to y if and only if there exists some x prime such that x evaluates to x prime and x prime Kleene-evaluates to y)

### Types of Expressions

Expressions can be *open* or *closed*.

**Open expressions** are expressions which depend on external state (i.e. `x + 3` where `x` is not in scope). 

**Closed expressions** are self-contained and depend only on internal state (state internal to the expression).

- can be created with a `let` expression
- embedding an open expression into a closed expression can create an overall closed expression (e.g. attaching a `let x` expression above `x + 3`)

## Let expressions

```haskell
let v = e in e'
```

`v` is the variable
`e` and `e'` are the expressions

This results in a **substitution** where we replace all *free* occurrences of `e` inside `e'` with `v`.

For example:

```haskell
let x = 6 in x + 4 => [6/x] x + 4 = 6 + 4 => 10
    ^v  ^e   ^e'      ^ means "replace x with 6"
```

`let` forms a foundational block in Haskell whereby many other statements are, under the hood, `let` expressions just using a nicer syntax.

- ex: `x = 4` is just `let x = 4 in ...` under the hood

`let` allows *multiple bindings*:

```haskell
let x = 4
    y = 6
    z = 10
in x + y + z

is equivalent to

let x = 4 in
    let y = 6 in
        let z = 10 in
            x + y + z
```

## Whitespace

Like Python, Haskell allows indentation and whitespace for its syntax.

Rules of indentation:

1. Subparts indented further
2. Grouped expressions must have the same indentation (i.e. be aligned)

Unlike Python, Haskell does not only use whitespace. For example, we can also write:

```haskell
let {x = 4; y = 6; z = 10} in x + y + z
```

For readability we prefer whitespace. For terseness we prefer semicolons.

## Lambda expressions

Lambda expressions are a simple way to define closede expressions relying on substitution. They use a single-line arrow `->` instead of double `=>`.

```haskell
\v -> e
 ^    ^expression
 ^variable
```

The backward slash `\` is supposed to resemble the Greek lowercase lambda. 

```haskell
(\v -> e) e' => [e'/v] e
```

- replace all instances of v with e'

For example:

```haskell
(\x -> x + 4) 3 => [3/x]x+4 = 3 + 4 => 7
 ^v    ^e     ^e'  ^replace x with 3
```

The dollar sign `$` is *right-associative*.

**Without** the dollar sign, Haskell is naturally left-associative:

```haskell
f a = ((f a) b)
^ ^argument
^function

sqrt 4 + 12 => 2 + 12 => 14
```

**With** the dollar sign, function application is right-associative:

```haskell
f$a = (f $ (a b))

sqrt $ 4 + 12 => sqrt 16 => 4
```

## Case expressions

Haskell uses a `case...of` syntax.

```haskell
case e'' of p  -> e
            p' -> e'
```

where `p` are **patterns** and `e` are expressions

Let's see a simple example using Boolean values. We will be glossing over the details of type definition but the syntax is fairly straightforward.

```haskell
data Bool = True | False
            ^expr  ^expr

case True of True -> 1      => 1
             False -> 2
             ^PATTERNS,
             NOT boolean statements
```

Pattern matching can also do variable substitution and binding, which we will see later.

### Combining lambdas and case expressions

```haskell
data Shape = Rock | Paper | Scissors

f = \s -> case s of Rock     -> 1
                    Paper    -> 2
                    Scissors -> 3

f Paper => case Paper of ... => 2
```

## Review

We've covered `let`, lambda, and case expressions, which let us build up larger programs.
