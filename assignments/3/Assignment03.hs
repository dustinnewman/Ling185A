module Assignment03 where

import RegEx
import SLG

import Data.List (nub)

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- IMPORTANT: Please do not change anything above here.
--            Write all your code below this line.
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

-- | Compute bigrams for a list.
--
-- >>> bigrams []
-- []
--
-- >>> bigrams [1,2,3]
-- [(1,2),(2,3)]
--
-- >>> bigrams "cat"
-- [('c','a'),('a','t')]
--
-- >>> bigrams [1,2]
-- [(1,2)]
bigrams :: [a] -> [(a, a)]
bigrams [] = []
bigrams (x:[]) = []
bigrams (x:y:[]) = (x,y) : []
bigrams (x:y:r) = (x,y) : (bigrams (y:r))
{--
 - To compute a list of bigrams, we have a base case of
 - an empty list, which of course has no bigrams. If we
 - just have one element in the list, then we also have
 - no bigrams. If we have two elements, then we take them
 - and create a tuple from them and cons them to a list.
 - If we have more than two elements, we put the first two
 - into a tuple, and then use the rest of the list (plus the
 - second element) and call recursively.
 --}

-- | Flatten a list of bigram tuples.
--
-- >>> pretty []
-- []
-- 
-- >>> pretty [(1,2),(2,3)]
-- [1,2,3]
--
-- >>> pretty [('c','a'),('a','t')]
-- "cat"
--
-- >>> pretty [(1,2),(3,4)]
-- []
--
-- >>> pretty [('c','a'),('a','t'),('t','s'),('s','!')]
-- "cats!"
pretty :: (Eq a) => [(a, a)] -> [a]
pretty [] = []
pretty l = case isChained l of
            False -> []
            True -> case l of
                    (x:[]) -> [fst x, snd x]
                    (x:r) -> (fst x) : (pretty r)
{--
 - For pretty we have a base case of an empty list, in which
 - case we return an empty list since there are no possible
 - bigrams. Else, we have some list. Per the spec, if our
 - isChained function returns false, then we return the empty
 - list. If it returns true, then we have a well-formed bigram
 - list. In the case of a bigram list, we have two options:
 - either one element or multiple. If just one, then take both
 - elements of the one tuple. If multiple, take the first element
 - of the first tuple, and then recurse.
 --}

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

-- | Returns the list of second element tuples in transitions.
--
-- >>> follows g2 "cat"
-- []
--
-- >>> follows g2 "the"
-- ["cat","very","fat"]
--
-- >>> follows g1 V
-- [C,V]
follows :: (Eq sy) => SLG sy -> sy -> [sy]
follows (_,_,[]) s = []
--follows (b,f,h:t) s = case (fst h) == s of {True -> (snd h) : (follows (b,f,t) s); False -> (follows (b,f,t) s)}
follows (_,_,g) s = map snd (filter (\x -> (fst x) == s) g)
{--
 - For follows, we have a list of transitions, which is a
 - list of tuples, defining (first, second) bigrams. We
 - see if the given string is the first element of any of
 - the bigrams. If so, we cons it onto the recursive call.
 - If not, then we just recurse on the rest of the transition
 - list.
 --}

-- | Returns the list of first element tuples in transitions.
--
-- >>> precedes g1 V
-- [C,V]
--
-- >>> precedes g2 "the"
-- []
--
-- >>> precedes g2 "cat"
-- ["the","fat"]
precedes :: (Eq sy) => SLG sy -> sy -> [sy]
precedes (_,_,[]) s = []
--precedes (b,f,h:t) s = case (snd h) == s of {True -> (fst h) : (precedes (b,f,t) s); False -> (precedes (b,f,t) s)}
precedes (_,_,g) s = map fst (filter (\x -> (snd x) == s) g) 
{--
 - Precedes has the exact same logic of follows, but testing
 - the second element of the tuple instead of the first.
 --}

-- | Returns list of substrings that start with x
-- and take up to n transitions in g.
--
-- >>> forward g1 1 C
-- [[C],[C,V]]
--
-- >>> forward g1 1 V
-- [[V],[V,C],[V,V]]
--
-- >>> forward g1 2 C
-- [[C],[C,V,C],[C,V,V],[C,V]]
--
-- >>> forward g2 1 "the"
-- [["the","cat"],["the","very"],["the","fat"]]
-- >>> forward g2 2 "very"
-- [["very","very","very"],["very","very","fat"],["very","fat","cat"],["very","very"],["very","fat"]]
forward :: (Eq sy) => SLG sy -> Int -> sy -> [[sy]]
forward g 1 x = [[x]]++(map (\y -> x:y:[]) (follows g x))
forward g n x = 
    nub (
        (forward g (n - 1) x)++(concat (
            map 
            (\s -> map 
                    (\n -> s++[n]) 
                    (follows g (last s))
            ) 
            (forward g (n - 1) x)
        ))
    )
{- forward starts by taking all possible transitions
 - that require just one step. From there, we need to
 - see which of THOSE transitions can be expanded
 - (n - 1) times. e.g. if we take `forward g2 2 "the"'
 - then our first batch of transitions is [["the", "cat"],
 - ["the","very"],["the","fat"]]. From there, we can expand
 - "very" into both "very" and "fat."
 - Programmatically, we start with our recursive case,
 - as any transition which takes (n-1) steps also takes at most
 - (n) steps. From there, we map all of those transitions
 - and see which ones have further transitions e.g. "very"
 - has both "very" and "fat." This is our call to `follows g
 - (last s)' where s = the list containing "very" as its last
 - element. This gets us all elements that follow "very" i.e.
 - ["very","fat"]. We then concatenate these two elements onto
 - our recursive result containing ["the","very"] to show that
 - these two transitions are taken from ["the","very"] state.
 - We take the entire result and run it through `nub' to remove
 - all duplicates.
 -}

-- MORE EXAMPLE USAGE:
-- backward g2 1 "cat"
-- => [["cat"],["the","cat"],["fat","cat"]]
-- backward g2 2 "very"
-- => [["very"],["the","very","very"],["very","very","very"],["the","very"],
--    ["very","very"]]

backward :: (Eq sy) => SLG sy -> Int -> sy -> [[sy]]
backward g 1 x = [[x]]++(map (:x:[]) (precedes g x))
backward g n x = nub ((backward g (n - 1) x)++(concat (map (\s -> map (\n -> [n]++s) (precedes g (head s))) (backward g (n - 1) x))))
{- backward follows the exact same logic as `forward' with
 - the exception of calling `precedes' rather than `follows'
 - and using the head element of the state transitons rather
 - than the last (since we are working backwards). We also 
 - prepend the list of state transitions (`[n]') to the
 - recursive case, for similar reasons.
 -}

generates :: (Eq sy) => SLG sy -> Int -> [[sy]]
generates g@(s,e,_) n = filter (\x -> elem (last x) e) (concat (map (forward g n) s))
{- generates returns the list of all valid strings a given grammar
 - `g' generates. Since we already have our `forward` function
 - implemented, we call that with the same arguments passed. This
 - gives us all possible strings generated in at most n steps.
 - However, this does not mean all those strings are valid. e.g.
 - `forward g2 2 "the"' returns ["the","very","very"] which is
 - not a valid string in g2, since it does not end in "cat."
 - So, we just use `filter' to get the states that do end in valid
 - ending symbols (i.e. elem (last x) e) i.e. the list of valid
 - strings in `g'.
 -}

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

occurrences :: Int -> (RegEx a) -> (RegEx a)
occurrences 0 a = One
occurrences 1 a = a
occurrences n a = Concat a (occurrences (n - 1) a)
{- occurrences has the base case of 0, in which case we
 - use the empty string One which accepts nothing (literally
 - accepts only the empty string i.e. nothing).
 - Also, if we have a 1 then we simply return the string
 - itself, which matches exactly 1 occurrence of itself.
 - Else, we concatenate the regex with the recursive call
 - to match n - 1 occurrences.
 -}

optional :: (RegEx a) -> (RegEx a)
optional a = Alt One a
{- optional is simply the alternation of either one, which
 - matches 0 occurrences of anything, or the regex itself,
 - which will match one occurrence of itself.
 -}

