module Assignment01 where

data WFF = T | F | Neg WFF
         | Conj WFF WFF | Disj WFF WFF deriving Show

------------------------------------------------------------------
------------------------------------------------------------------
-- IMPORTANT: Please do not change anything above here.
--            Write all your code below this line.

denotation :: WFF -> Bool
denotation T = True
denotation F = False
denotation (Neg T) = False
denotation (Neg F) = True
denotation (Neg x) = case (denotation x) of {True -> False; False -> True}
denotation (Conj x y) = case (denotation x) of {True -> (denotation y); False -> False}
denotation (Disj x y) = case (denotation x) of {True -> True; False -> (denotation y)}

