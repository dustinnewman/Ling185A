module Assignment02 where

-- Imports just a few things that we have seen from the standard Prelude
-- module. (If there is no explicit 'import Prelude' line, then the entire
-- Prelude module is imported.)
import Prelude((+), (-), (*), (<), (>), (++), not, Bool(..), Char, undefined)

import Recursion

------------------------------------------------------------------
------------------------------------------------------------------
-- IMPORTANT: Please do not change anything above here.
--            Write all your code below this line.

times :: Numb -> Numb -> Numb
times = undefined

equal :: Numb -> Numb -> Bool
equal = undefined

bigger :: Numb -> Numb -> Numb
bigger = undefined

count :: (a -> Bool) -> [a] -> Numb
count = undefined

remove :: (a -> Bool) -> [a] -> [a]
remove = undefined

prefix :: Numb -> [a] -> [a]
prefix = undefined

depth :: WFF -> Numb
depth = undefined
