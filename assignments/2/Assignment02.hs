module Assignment02 where

-- Imports just a few things that we have seen from the standard Prelude
-- module. (If there is no explicit 'import Prelude' line, then the entire
-- Prelude module is imported.)
import Prelude((+), (-), (*), (<), (>), (++), not, Bool(..), Char, undefined)

import Recursion

------------------------------------------------------------------
------------------------------------------------------------------
-- IMPORTANT: Please do not change anything above here.
--            Write all your code below this line.

times :: Numb -> Numb -> Numb
times E b = E -- Anything times zero is zero
times a E = E
times (S a) (S b) = (add (S b) (times a (S b)))
-- Multipication is simply addition of the second
-- operand some amount of times, where the amount
-- of times is specified by the first operand.
-- So, we add the second operand to itself through
-- recursive calls, decrementing the first operand
-- until we have 0 i.e. no more addition required

equal :: Numb -> Numb -> Bool
equal E E = True -- Zero is equal to itself
equal E b = False -- Base cases for recursive call
equal a E = False
equal (S a) (S b) = equal a b
-- This function operates by decrementing each
-- argument. The argument stems from the fact
-- that if n = m, then n - 1 = m - 1. We repeat
-- this procedure (performing "subtraction" by
-- destructuring the arguments through pattern
-- matching) until one of the arguments is zero.
-- In that case, if they are both zero, then
-- the two must be equal. If they are not, then
-- the two arguments were not equal.

bigger :: Numb -> Numb -> Numb
bigger E b = b -- Base cases. Anything is bigger
bigger a E = a -- than zero.
bigger (S a) (S b) = S (bigger a b)
-- We repeatedly subtract from each argument.
-- If n > m, then n - 1 > m - 1. However, we
-- must make sure to re-add the digit we took
-- off i.e. the S for each call, or else we will
-- always get one (or zero if they are equal).

count :: (a -> Bool) -> [a] -> Numb
count f [] = E -- There is nothing to count
count f (h : t) = case f h of {True -> (add (S E) (count f t)); False -> (count f t)}
-- We apply the function to the first argument
-- of the list (I use 'h' to denote the head,
-- a practice from OCaml, instead of 'x') and
-- since the function is boolean, we either
-- get True or False back. If True, then add one
-- to our running total. If not, then count
-- the rest of the list and just return that.

remove :: (a -> Bool) -> [a] -> [a]
remove f [] = []
remove f (h : t) = case f h of {True -> (remove f t); False -> h : (remove f t)}
-- This function uses very similar logic to the
-- above count function. Basically, we have a
-- base case of returning the empty list if
-- we receive the empty list, as there is nothing
-- to remove. If we have an element, we see if
-- the function returns true for it. If so, we do
-- not keep it and recurse the rest of the list.
-- If not, then we keep the element and cons that
-- onto the recursive return value.

prefix :: Numb -> [a] -> [a]
prefix E l = [] -- Zero elements of any list is the empty list.
prefix (S a) [] = [] -- No elements to return.
prefix (S a) (h : t) = h : (prefix a t)
-- We decrement n for each call of the function.
-- Each time, we cons the head of the list to
-- the rest of the recursive result. Once we have
-- decremented n to 0, we hit the base case and
-- return the empty list.

depth :: WFF -> Numb
depth T = (S E) -- An item with no children has length one.
depth F = (S E)
depth (Neg a) = add (S E) (depth a)
-- Neg takes only one child, so we add one for
-- the Neg and then count the rest
depth (Conj a b) = add (S E) (bigger (depth a) (depth b))
depth (Disj a b) = add (S E) (bigger (depth a) (depth b))
-- We take the larger of the two branches for a
-- Conj or a Disj and add one to it (for the 
-- Conj/Disj itself).

